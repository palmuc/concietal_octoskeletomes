
'''

This script calculated relative abundance for single target aminoacid
in proteins sequences. A proteins input file (fasta format) and the 
single letter code for the aminoacid are required.
Output is a csv file reporting relative abundance (% format) values
for each sequence in the input file. It requires Biopython installed 
on your system.

'''



# import relevant modules
import sys
import Bio
import argparse
import warnings
from argparse import RawTextHelpFormatter
from Bio import SeqIO,Entrez
from Bio.SeqUtils.ProtParam import ProteinAnalysis


# Two arguments are required to run the script:
# 1) file containing protein sequences (fasta format) (positional)
# 2) single letter code for the amino acid of interest (optional)

parser = argparse.ArgumentParser(usage='', description='', formatter_class=RawTextHelpFormatter)
parser.add_argument( 'input_file', help = 'Your input file (fasta format) \n\n')
parser.add_argument( '-aa', dest='target_aminoacid', help = 'The amino acid for which to calculate abundance \n\n', required=True, type = str )
args = parser.parse_args()


# Define formatting for warning message in case of incomplete sequences provided. 

def incomplete_seqs_warning(msg, *args, **kwargs):
    return '\n\n' + str(msg) + '\n\n'

warnings.formatwarning = incomplete_seqs_warning


# Creates a list to store relative abundance values for the target amino acid.

Aminoacid_frequency_persequence = []

# Fasta file is parsed with Biopython SeqIO.


for seqrecord in SeqIO.parse(args.input_file, 'fasta'):
    
# Input file is examined for possible incomplete sequences. Check each seqrecord for starting methionine and ending stop codon.
# If one or both conditions are not met, a warning issued. 
    
    if seqrecord.seq[0] != 'M' or seqrecord.seq[len(seqrecord.seq)-1] != '*':
        warnings.warn('  Warning! Some sequences appear incomplete. This might lead to biased abundance values.')
    else:
        pass

# Aminoacidic composition for each sequences is determined.         
        
    analysed_Seq = ProteinAnalysis(str(seqrecord.seq))
    
# The relative abundance for the target aminoacid is calculated.Values are stored (as percentages, e.g. 5.43) in the list Aminoacid_frequency_persequence.

    aminoacid_frequency = (float(analysed_Seq.count_amino_acids()[args.target_aminoacid]) / len(str(seqrecord.seq)))*100
    
    if args.minimum_frequency:
        
        if aminoacid_frequency > args.minimum_frequency:
            Aminoacid_frequency_persequence.append(aminoacid_frequency)
    else:
        Aminoacid_frequency_persequence.append(aminoacid_frequency)


# Output csv file is created (one value is printed per line). 

with open( 'Abundance.csv','w' ) as f:
    for i in Aminoacid_frequency_persequence:
        f.write('%f\n' % i)

sys.stderr.write('\n  Done! Abudance values are store in Abundance.csv')
