
'''

This scripts retrieved sequences from fasta file based on the 
relative abundance of a single target aminoacid. A proteins input
file (fasta format),the single letter code for the aminoacid, and
the relative abundance cut-off value are required.
Output is a fasta file with the sequences that match the searc
criteria. It requires Biopython installed on your system.


'''

#Import all requried stuff

import sys
import Bio
import argparse
import warnings
from argparse import RawTextHelpFormatter
from Bio import SeqIO,Entrez
from Bio.SeqUtils.ProtParam import ProteinAnalysis


# Two arguments are required to run the script:
# 1) file containing protein sequences (fasta format) (positional)

parser = argparse.ArgumentParser(usage='', description='', formatter_class=RawTextHelpFormatter)
parser.add_argument( 'input_file', help = 'Your input file (fasta format) \n\n')
parser.add_argument( '-aa', dest='target_aminoacid', help = 'Minimum abundance value for the target aminoacid', type=str, required=True)
parser.add_argument( '-f', dest='aminoacid_frequency', help = 'Minimum abundance of aspartic acid', type=float, required=True)
args = parser.parse_args()


# Define formatting for warning message in case of incomplete sequences provided. 

def incomplete_seqs_warning(msg, *args, **kwargs):
    return '\n\n' + str(msg) + '\n\n'

warnings.formatwarning = incomplete_seqs_warning

# Creates a list to store protein sequences (as Biopython SeqRecord objects) matching the sear criteria

extracted_sequences = []

# Initialise two counters. Total_sequences_counter records the number of sequences parsed by SeqIO.parse
# Complete_sequences_counter records the number of complete seqeuences that are processed.

total_sequences_counter = 0
complete_sequences_counter = 0

# Fasta file is parsed with Biopython SeqIO. Counte

for seqrecord in SeqIO.parse(args.input_file, 'fasta'):
    total_sequences_counter +=1

    if seqrecord.seq[0] == 'M' and seqrecord.seq[len(seqrecord.seq)-1] == '*':
        complete_sequences_counter +=1
        
# Aminoacidic composition for sequences in the input file is computed and the relative
# abundance of the target aminoacid calculated. 
        
        analysed_Seq = ProteinAnalysis(str(seqrecord.seq))
        aminoacid_percentage = (float(analysed_Seq.count_amino_acids()[args.target_aminoacid]) / len(str(seqrecord.seq)))
    
# Sequences with a relative abundance value for the target amino acid are appended to the extracted_Sequences_list.     
    
        if aminoacid_percentage > args.aminoacid_frequency:
            extracted_sequences.append(seqrecord)
    

# Output file (fasta format) is created using the Biopython SeqIO.write module

output_file = SeqIO.write(extracted_sequences, 'Extracted_proteins.fasta', 'fasta')


sys.stderr.write('\n  Done! Abudance values are store in Extracted_proteins.fasta')

# If incomplete sequences were present in the input file (total_sequences_counter > complete_sequences_counter),
# a warning message is printed on the console. 

if total_sequences_counter > complete_sequences_counter:
     warnings.warn('  Warning! Some input sequences appear incomplete and were excluded from the analysis.')
