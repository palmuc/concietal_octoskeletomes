### New non-bilaterian transcriptomes provide novel insights into the evolution of coral skeletomes.
#### Nicola Conci, Gert Wörheide & Sergio Vargas

If you use these data, please cite:

**Nicola Conci, Gert Wörheide, Sergio Vargas, New Non-Bilaterian Transcriptomes Provide Novel Insights into the Evolution of Coral Skeletomes, Genome Biology and Evolution, Volume 11, Issue 11, November 2019, Pages 3068–3081, https://doi.org/10.1093/gbe/evz199**

This repository contains the following folders/files:

* supplementary_figures/
	- This folder contains supplementary figures mentioned in the manuscript
* supplementary_materials/
	- This folder contains supplementary materials mentioned in the manuscript
* alignments\_and\_treefiles/
	- This folder contains untrimmed and trimmed alignments used for phylogenetic inference and the resulting tree files for the different analyses
* scripts/
	- python scripts used for analysis of aminoacid frequencies
* metatranscriptome_assemblies/
	- Heliopora\_coerulea\_Meta-assembly.fasta.gz
	- Pinnigorgia\_flava\_Meta-assembly.fasta.gz
	- Sinularia\_cf\_cruciata\_Meta-assembly.fasta.gz
	- Tubipora\_musica\_Meta-assembly.fasta.gz
	- host\_only\_predictedCDSs/
		+ this folder includes host only predicted CDSs
* README.md

To clone this repository and get the assemblies you need to install and configure git-lfs (https://git-lfs.github.com/). If git-lfs is not installed you are NOT going to get the actual assembly files, but pointers to them.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
